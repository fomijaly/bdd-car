const express = require("express");
const router = express.Router();

const connection = require("../config/db");

//Récupérer la liste des voitures
router.get("/", (req, res) => {
  connection.execute("SELECT * FROM car", (err, results) => {
    if (err) {
      res.status(500).send("Erreur lors de la récupération des voitures");
    } else {
      res.json(results);
    }
  });
});

//Afficher les voitures d'une certaine marque
router.get("/:brand", (req, res) => {
  const brand = req.params.brand;
  connection.execute(
    "SELECT * FROM car WHERE brand = ?",
    [brand],
    (err, results) => {
      if (err) {
        res
          .status(500)
          .send(
            `Erreur lors de la récupération des voitures de la marque ${brand}`
          );
      } else {
        res.json(results);
      }
    }
  );
});

//Ajouter une voiture
router.post("/", (req, res) => {
  const { brand, model } = req.body;
  connection.execute(
    "INSERT INTO car (brand, model) VALUES (? , ?)",
    [brand, model],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de l'ajout de la voiture");
      } else {
        res
          .status(201)
          .send(`La voiture avec l'ID ${results.insertId} a été ajoutée.`);
      }
    }
  );
});

//Modifier une voiture
router.put("/:id", (req, res) => {
  const { brand, model } = req.body;
  const id = req.params.id;
  connection.execute(
    "UPDATE car SET brand = ?, model = ? WHERE car_id = ?",
    [brand, model, id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la mise à jour de la voiture");
      } else {
        res.send("La voiture a été mise à jour");
      }
    }
  );
});

//Supprimer une voiture
router.delete("/:id", (req, res) => {
  const id = req.params.id;
  connection.execute(
    "DELETE FROM car WHERE car_id = ?",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la suppression de la voiture");
      } else {
        res.send("Voiture supprimée");
      }
    }
  );
});

module.exports = router;