const express = require("express");
const router = express.Router();
const { validateGarageData } = require('../middlewares/validator.middleware');
const connection = require("../config/db.js");

//Afficher tous les garages
router.get("/", (req, res) => {
  connection.query("SELECT * FROM garage", (err, results) => {
    if (err) {
      res.send({ error: err.message });
    } else {
      res.json(results);
    }
  });
});

// Afficher un garage selon son id
router.get("/:id", (req, res) => {
  const id = req.params.id;
  connection.query(
    "SELECT * FROM garage WHERE garage_id = ?",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err.message });
      } else {
        res.send(results);
      }
    }
  );
});

//Ajouter créer un nouveau garage
router.post("/", validateGarageData, (req, res) => {
  const { name, email } = req.body;
  connection.execute(
    "INSERT INTO garage (name, email) VALUES (?, ?)",
    [name, email],
    (err, results) => {
      if (err) {
        res.status(500).send({message: err.message, error: err});
      } else {
        const data = req.body;
        res.json(data);
      }
    }
  );
});

//Supprimer un garage
router.delete("/:id", (req, res) => {
  const id = req.params.id;
  connection.execute(
    "DELETE FROM garage WHERE garage_id = ? ",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err.message });
      } else {
        res.status(201).send("Garage supprimé");
      }
    }
  );
});

//Mettre à jour un garage
router.put("/:id", validateGarageData, (req, res) => {
  const id = req.params.id;
  const { name, email } = req.body;
  connection.execute(
    "UPDATE garage SET name = ?, email = ? WHERE garage_id = ? ",
    [name, email, id],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err.message });
      } else {
        const data = req.body;
        res.send(data);
      }
    }
  );
});

module.exports = router;
