const validateEmail = (mail) => {
    const mailFormat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (mail.match(mailFormat)) {
    return true;
  } else {
    console.log("Il y a une erreur dans la saisie de l'adresse mail");
    return false;
  }
};

const validateGarageData = (req, res, next) => {
  const { name, email } = req.body;
  if ((!name || !email) || !validateEmail(email)) {
    return res.status(400).json({message: 'Il y a une erreur'});
  }
  next();
};

module.exports = { validateGarageData };
