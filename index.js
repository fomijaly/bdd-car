require("dotenv").config();
const express = require("express");
const cors = require("cors");
const app = express();
const bcrypt = require("bcrypt");

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(cors({ origin: "http://localhost:8080" }));

const { logRequest } = require("./middlewares/logger.middleware");
app.use(logRequest);

const carsController = require("./controllers/cars.controller");
const garageController = require("./controllers/garages.controller");
const connection = require("./config/db");

app.use("/cars", carsController);
app.use("/garages", garageController);

app.post("/register", (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.status(400).json({ error: "Please specify both email and password" });
  } else {
    bcrypt.hash(password, 10, (bcryptError, hashedPassword) => {
      if (bcryptError) {
        res.status(500).json({ error: bcryptError.message });
      } else {
        connection.execute(
          `INSERT INTO user(email, password) VALUES (?,?)`,
          [email, hashedPassword],
          (mysqlError, results) => {
            if (mysqlError) {
              res.status(500).json({ error: mysqlError.message });
            } else {
              res.status(201).json({
                id: results.insertId,
                email,
              });
            }
          }
        );
      }
    });
  }
});

app.post("/login", (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res
      .status(400)
      .json({ errorMessage: "Please specify both email and password" });
  } else {
    connection.query(
      `
    SELECT * 
    FROM user
    WHERE email = ?
    `,
      [email],
      (mysqlError, results) => {
        if (mysqlError) {
          res.status(500).json({ error: mysqlError.message });
        } else if (results.length === 0) {
          res.status(401).json({ error: "Email invalide" });
        } else {
          const user = results[0];
          const hashedPassword = user.password;
          bcrypt.compare(
            password,
            hashedPassword,
            (bcryptError, passwordMatch) => {
              if (bcryptError) {
                res.status(500).json({ error: bcryptError.message });
              } else if (passwordMatch) {
                res.status(200).json({
                  id: user.id,
                  email: user.email,
                });
              } else {
                res.status(401).json({ error: "Mot de passe invalide" });
              }
            }
          );
        }
      }
    );
  }
});

app.get("/users", (req, res) => {
  connection.query("SELECT * from user", (err, results) => {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      res.json(results);
    }
  });
});

const port = 3000;
app.listen(port, () => {
  console.log(`Le serveur est écouté sur le port ${port}`);
});
